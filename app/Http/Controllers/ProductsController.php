<?php namespace App\Http\Controllers;

use Samplecompany\Interactors\ProductPurchasingInteractor;

use Samplecompany\Exceptions\NoStockException;
use Samplecompany\Exceptions\NotEnoughBalanceException;

class ProductsController extends Controller {

    /**
     * @var ProductPurchasingInteractor
     */
    private $productPurchasingInteractor;

    /**
     * Create a new controller instance.
     *
     * @param ProductPurchasingInteractor $productPurchasingInteractor
     */
    public function __construct(ProductPurchasingInteractor $productPurchasingInteractor)
    {
        $this->middleware('auth');
        $this->productPurchasingInteractor = $productPurchasingInteractor;
    }

    /**
     * Show the application dashboard to the user.
     *
     * @param int $id
     * @return Response
     */
    public function purchase($id)
    {
        $userId = \Auth::user()->id;

        try {
            $this->productPurchasingInteractor->purchase($userId, $id);

            // return success
        } catch (NoStockException $e) {
            // return error there is no stock to be purchased
        } catch (NotEnoughBalanceException $e) {
            // return error there is not enough money in user account
        }
    }

}
