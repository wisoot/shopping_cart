<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider {

    public function register() {

        $this->app->bind(
            "Samplecompany\\Repositories\\Interfaces\\ProductsRepositoryInterface",
            "Samplecompany\\Repositories\\ProductsRepository"
        );

        $this->app->bind(
            "Samplecompany\\Repositories\\Interfaces\\UserAccountsRepositoryInterface",
            "Samplecompany\\Repositories\\UserAccountsRepository"
        );

    }

}