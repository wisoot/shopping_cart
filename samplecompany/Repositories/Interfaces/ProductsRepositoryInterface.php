<?php namespace Samplecompany\Repositories\Interfaces;

use Samplecompany\Entities\Product;

interface ProductsRepositoryInterface {

    /**
     * fetchById method
     *
     * @param int $id
     * @return Product
     */
    public function fetchById($id);

    /**
     * decreaseNumberOfItem method
     *
     * @param int $id
     * @return bool
     */
    public function decreaseNumberOfItem($id);

}