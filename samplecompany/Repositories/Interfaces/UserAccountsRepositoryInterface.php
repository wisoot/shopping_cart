<?php namespace Samplecompany\Repositories\Interfaces;

use Samplecompany\Entities\UserAccount;

interface UserAccountsRepositoryInterface {

    /**
     * fetchById method
     *
     * @param int $id
     * @return UserAccount
     */
    public function fetchById($id);

    /**
     * deductAmount method
     *
     * @param int $id
     * @param int $amount
     * @return bool
     */
    public function deductAmount($id, $amount);

}