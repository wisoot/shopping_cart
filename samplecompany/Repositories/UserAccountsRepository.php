<?php namespace Samplecompany\Repositories;

use Samplecompany\Entities\UserAccount;
use Samplecompany\Repositories\Interfaces\UserAccountsRepositoryInterface;

class UserAccountsRepository extends Repository implements UserAccountsRepositoryInterface {

    /**
     * fetchById method
     *
     * @param int $id
     * @return UserAccount;
     */
    public function fetchById($id) {
        //
    }

    /**
     * deductAmount method
     *
     * @param int $id
     * @param int $amount
     * @return bool
     */
    public function deductAmount($id, $amount) {
        //
    }

}