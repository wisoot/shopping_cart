<?php namespace Samplecompany\Repositories;

use Samplecompany\Entities\Product;
use Samplecompany\Repositories\Interfaces\ProductsRepositoryInterface;

class ProductsRepository extends Repository implements ProductsRepositoryInterface {

    /**
     * fetchById method
     *
     * @param int $id
     * @return Product;
     */
    public function fetchById($id) {
        //
    }

    /**
     * decreaseNumberOfItem method
     *
     * @param $id
     * @return bool
     */
    public function decreaseNumberOfItem($id) {
        //
    }

}