<?php namespace Samplecompany\Interactors;

use Samplecompany\Repositories\Interfaces\ProductsRepositoryInterface as ProductsRepository;
use Samplecompany\Repositories\Interfaces\UserAccountsRepositoryInterface as UserAccountsRepository;

use Samplecompany\Exceptions\NoStockException;
use Samplecompany\Exceptions\NotEnoughBalanceException;

class ProductPurchasingInteractor extends Interactor {

    /**
     * @var ProductsRepository
     */
    private $productsRepository;

    /**
     * @var UserAccountsRepository
     */
    private $userAccountsRepository;

    /**
     * Class constructor
     *
     * @param ProductsRepository $productsRepository
     * @param UserAccountsRepository $userAccountsRepository
     */
    public function __construct(ProductsRepository $productsRepository, UserAccountsRepository $userAccountsRepository) {
        $this->productsRepository = $productsRepository;
        $this->userAccountsRepository = $userAccountsRepository;
    }

    /**
     * purchase method
     *
     * @param $userId
     * @param $productId
     * @return bool
     * @throws NoStockException
     * @throws NotEnoughBalanceException
     */
    public function purchase($userId, $productId) {
        $product = $this->productsRepository->fetchById($productId);
        $userAccount = $this->userAccountsRepository->fetchById($userId);

        if ($product->numberOfItem <= 0) {
            throw new NoStockException;
        }

        if ($product->price > $userAccount->balance) {
            throw new NotEnoughBalanceException;
        }

        $this->productsRepository->decreaseNumberOfItem($productId);
        $this->userAccountsRepository->deductAmount($userId, $product->price);

        return true;
    }

}