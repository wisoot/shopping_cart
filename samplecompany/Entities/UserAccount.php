<?php namespace Samplecompany\Entities;

class UserAccount {

    /**
     * @var int - user account id
     */
    public $id;

    /**
     * @var int - user id
     */
    public $userId;

    /**
     * @var int - user account balance in cent
     */
    public $balance;

}