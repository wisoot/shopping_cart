<?php namespace Samplecompany\Entities;

class Product {

    /**
     * @var int - product id
     */
    public $id;

    /**
     * @var string - product name
     */
    public $name;

    /**
     * @var int - number of items left in the stock
     */
    public $numberOfItem;

    /**
     * @var int - price in cent
     */
    public $price;

}